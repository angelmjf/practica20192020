#include <stdio.h>
#include <math.h>
#include <omp.h>
#include <sys/time.h>
#include "wtime.h"
#include "definitions.h"
#include "energy_struct.h"
#include "solver.h"

/**
* Funcion que implementa la solvatacion en openmp
*/
extern void forces_OMP_AU (int atoms_r, int atoms_l, int nlig, float *rec_x, float *rec_y, float *rec_z, float *lig_x, float *lig_y, float *lig_z, float *ql ,float *qr, float *energy, int nconformations){

//  printf(" En el fichero solver_omp.cpp se encuentra la funcion forces_omp_au que se debe implementar con la version OpenMP\n");


  float dist, total_elec = 0, miatomo[3], elecTerm;
  int totalAtomLig = nconformations * nlig;

  int k = 0, i = 0, j = 0;

  //int numHilos = 48;
  int numHilos = omp_get_max_threads();

  // Ejecuta con el numero maximo de hilos
  omp_set_num_threads(numHilos);
  printf("\n\n Numero Total hilos en ejecución: %d \n ", numHilos);
  
  // primero 1/1 más rápido
  #pragma omp parallel for private(dist, miatomo, elecTerm, k, i, j) reduction(+:total_elec)  
  // cuarto 1/2 #pragma omp parallel for private(dist, miatomo, elecTerm, k, i, j)
  // tercero 1/3 más rápido
  //#pragma omp parallel for private( k)
   
  
  // segundo 1/1 más rápido
  //#pragma omp parallel for private(dist, miatomo, elecTerm) reduction(+:total_elec)
  for ( k=0; k < totalAtomLig; k+=nlig) {
    // tercero 2/3 más rápido
    //#pragma omp parallel for private(i, miatomo) 
    for( i=0;i<atoms_l;i++){
      miatomo[0] = *(lig_x + k + i);
      miatomo[1] = *(lig_y + k + i);
      miatomo[2] = *(lig_z + k + i);
        // tercero 3/3 más rápido
        //#pragma omp parallel for private (elecTerm, dist, j) reduction(+:total_elec)              
        // cuarto 2/2 más rapido
        // #pragma omp parallel reduction(+:total_elec)
        for( j=0;j<atoms_r;j++){
          elecTerm = 0;
          dist=calculaDistancia (rec_x[j], rec_y[j], rec_z[j], miatomo[0], miatomo[1], miatomo[2]);
          elecTerm = (ql[i]* qr[j]) / dist;
          total_elec += elecTerm;
        }
     }
     energy[k/nlig] = total_elec;
     total_elec = 0;
  }
  printf("Termino electrostatico %f\n", energy[0]);
}



